<!DOCTYPE>
<html lang="ru">
<head>
    <title>Repository CD</title>

    <meta charset="utf-8">

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="js/toast/toastr.css">
    <link rel="stylesheet" href="js/toast/toastr.style.css">

    <link rel="stylesheet" href="css/compiled/style.css">
</head>
<body>
<div class="container-fluid">
    <div id="wrapper">

        <div class="header">
            <div class="d-flex justify-content-between align-items-center">

                <div class="d-flex align-items-center">
                    <img src="https://upload.wikimedia.org/wikipedia/commons/0/01/Faenza-application-x-cd-image.svg" class="logo mr-2">
                    <button class="btn btn-success" data-toggle="modal" data-target="#create-album">
                        Добавить альбом
                    </button>
                </div>
            </div>
        </div>

        <div class="body">
            <table id="albums-table" class="table table-striped" data-limit="<?=$page_limit?>">
                <thead>
                <tr>
                    <th class="sorting-disabled">Обложка</th>
                    <th>Название</th>
                    <th>Артист</th>
                    <th>Год выпуска</th>
                    <th>Длительность (мин)</th>
                    <th>Дата покупки</th>
                    <th>Стоимость</th>
                    <th>Комната</th>
                    <th>Стойка</th>
                    <th>Полка</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                </tfoot>
            </table>

        </div>

        <?php include 'modal' . DIRECTORY_SEPARATOR . 'create_album.php'; ?>
        <?php include 'modal' . DIRECTORY_SEPARATOR . 'delete_album.php'; ?>
        <?php include 'modal' . DIRECTORY_SEPARATOR . 'update_album.php'; ?>
    </div>

</div>



<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<script src="js/toast/toastr.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
<script type="module" src="js/main.js"></script>

</body>
</html>