<div class="modal fade" id="update-album">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="?route=main" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title">Обновление альбома</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>
                            Название альбома
                            <input type="text" name="name" class="form-control">
                        </label>
                    </div>
                    <div class="form-group">
                        <label>
                            Артист
                            <input type="text" name="author" class="form-control">
                        </label>
                    </div>
                    <div class="form-group">
                        <label>
                            Год выпуска
                            <input type="number" name="release_year" class="form-control">
                        </label>
                    </div>
                    <div class="form-group">
                        <label>
                            Продолжительность
                            <input type="number" name="duration" class="form-control">
                        </label>
                    </div>
                    <div class="form-group">
                        <label>
                            Дата покупки
                            <input type="date" name="bought_at" class="form-control">
                        </label>
                    </div>
                    <div class="form-group">
                        <label>
                            Стоимость покупки
                            <input type="number" name="price" class="form-control">
                        </label>
                    </div>
                    <div class="form-row">
                        <div class="col">
                            <label>
                                Номер комнаты
                                <input type="text" name="room" class="form-control">
                            </label>
                        </div>
                        <div class="col">
                            <label>
                                Номер стойки
                                <input type="text" name="stand" class="form-control">
                            </label>
                        </div>
                        <div class="col">
                            <label>
                                Номер полки
                                <input name="shelf" class="form-control">
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>
                            Обложка
                            <input type="file" class="form-control-file" name="file">
                        </label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Отменить</button>

                    <input type="hidden" name="action" value="update">
                    <input type="hidden" name="id" value="0">
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </div>
            </form>
        </div>
    </div>
</div>