<div class="modal fade" id="delete-album">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="?route=main">
                <div class="modal-header">
                    <h5 class="modal-title">Удаление альбома</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Вы уверены, что хотите удалить эту запись?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Отменить</button>

                    <input type="hidden" name="action" value="delete">
                    <input type="hidden" name="id" value="0">
                    <button type="submit" class="btn btn-primary">Подтвердить</button>
                </div>
            </form>
        </div>
    </div>
</div>