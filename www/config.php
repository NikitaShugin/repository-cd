<?php

return [
    'db' => [
        'host' => 'mariadb',
        'user' => 'root',
        'password' => 'root',
        'port' => 3306,
        'dbname' => 'm1',
    ],
];