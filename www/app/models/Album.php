<?php

namespace M1\Models;

use M1\Core\Model;

class Album extends Model
{
    public $required = [
        'name', 'author', 'duration', 'price'
    ];

    protected $table = 'albums';
}