<?php

namespace M1\Controllers;

use M1\Core\Controller;
use M1\Models\Album;
use M1\Helpers\{Converter, Request, ImageUpload};

class MainController extends Controller
{
    public function init($id)
    {
        if (empty($id) || !is_numeric($id)) {
            $this->error('Ошибка работы системы. Был передан некорректный параметр.');
        }

        $model = new Album($id);

        if (!$model->exist()) {
            Request::error('Ошибка. Альбом не найден в базе.');
        }

        return $model;
    }

    public function index()
    {
        $model = new Album();

        $limit = $_POST['length'];
        $page = $_POST['start'] / $limit + 1;
        $albumsCount = $model->count();
        $sortColumnId = $_POST['order'][0]['column'];
        $sortColumnDirection = $_POST['order'][0]['dir'];
        $sortColumn = $_POST['columns'][$sortColumnId]['data'];
        $sort = !empty($sortColumn) ? "{$sortColumn} {$sortColumnDirection}" : false;

        $conditions = [];
        foreach ($_POST['columns'] as $data) {
            if (!empty($data['search']['value'])) {
                $conditions[$data['data']] = $data['search']['value'];
            }
        }

        $albums = $model->get([
            'id',
            'image',
            'name',
            'author',
            'release_year',
            'duration',
            'bought_at',
            'price',
            'room',
            'stand',
            'shelf'
        ], $conditions, $sort, ($page - 1) * $limit, $limit, true);

        Request::response([
            'draw' => $_POST['draw'],
            'data' => $albums,
            'recordsTotal' => $albumsCount,
            'recordsFiltered' => $albumsCount
        ]);
    }

    public function create()
    {
        $model = new Album();
        $errors = Request::checkErrors($model->required);

        if (!empty($errors)) {
            Request::error('Ошибка заполнения формы.', [
                'fields' => $errors
            ]);
        }

        $data = [
            'image' => !empty($_FILES['file']['name']) ? ImageUpload::upload() : null,
            'name' => Converter::toString($_POST['name']),
            'author' => Converter::toString($_POST['author']),
            'release_year' => !empty($_POST['release_year']) ? Converter::toInt($_POST['release_year']) : null,
            'duration' => Converter::toInt($_POST['duration']),
            'bought_at' => !empty($_POST['bought_at']) ? $_POST['bought_at'] : null,
            'price' => Converter::toFloat($_POST['price']),
            'room' => !empty($_POST['room']) ? Converter::toString($_POST['room']) : null,
            'stand' => !empty($_POST['stand']) ? Converter::toString($_POST['stand']) : null,
            'shelf' => !empty($_POST['shelf']) ? Converter::toString($_POST['shelf']) : null,
            'created_at' => date('Y-m-d H:i:s')
        ];

        $data['id'] = $model->create($data);

        Request::success('Задача успешно создана.', [
            'album' => $data
        ]);
    }

    public function update()
    {
        $album = $this->init($_POST['id']);
        $errors = Request::checkErrors($album->required);

        if (!empty($errors)) {
            Request::error('Ошибка заполнения формы.', [
                'fields' => $errors
            ]);
        }

        $album->update([
            'image' => !empty($_FILES['file']['name']) ? ImageUpload::upload() : null,
            'name' => Converter::toString($_POST['name']),
            'author' => Converter::toString($_POST['author']),
            'release_year' => !empty($_POST['release_year']) ? Converter::toInt($_POST['release_year']) : null,
            'duration' => Converter::toInt($_POST['duration']),
            'bought_at' => !empty($_POST['bought_at']) ? $_POST['bought_at'] : null,
            'price' => Converter::toFloat($_POST['price']),
            'room' => !empty($_POST['room']) ? Converter::toString($_POST['room']) : null,
            'stand' => !empty($_POST['stand']) ? Converter::toString($_POST['stand']) : null,
            'shelf' => !empty($_POST['shelf']) ? Converter::toString($_POST['shelf']) : null,
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Request::success('Задача успешно обновлена.');
    }

    public function delete()
    {
        $album = $this->init($_POST['id']);

        $album->delete();

        Request::success('Альбом успешно удалён.');
    }
}