<?php

namespace M1\Core;

use Exception;

class Controller
{
    protected $template;
    protected $view = [];

    public function __construct()
    {
        $action = $_POST['action'];

        if (!empty($action)) {
            if (!method_exists($this, $action)) {
                throw new Exception("Не найден обработчик для действия {$action}.");
            }

            $this->{$action}();
        }
    }

    public function render()
    {
        extract($this->view, EXTR_SKIP);

        $template = !empty($this->template) ? str_replace('.', DS, $this->template) : 'layout';

        ob_start();

        $filename = TEMPLATE_DIR . "{$template}.php";

        if (!file_exists($filename)) {
            throw new Exception("Не найден шаблон {$template}");
        }

        include $filename;

        die(ob_get_clean());
    }
}