<?php

namespace M1\Core;

use PDO;

class DB
{
    private static $pdo;

    public static function connect()
    {
        $config = include DIR.'config.php';

        self::$pdo = new PDO(
            "mysql:host={$config['db']['host']};dbname={$config['db']['dbname']}",
            $config['db']['user'],
            $config['db']['password'],
            [
                PDO::ATTR_STRINGIFY_FETCHES => false,
                PDO::ATTR_EMULATE_PREPARES => false,
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES "utf8";',
            ]
        );
    }

    public static function query($sql, $data = [])
    {
        $stmt = self::$pdo->prepare($sql);

        $stmt->execute($data);

        return $stmt;
    }

    public static function insert($sql, $data = [])
    {
        self::query($sql, $data);

        return self::$pdo->lastInsertId();
    }

    public static function select($sql, $data = [], $multiple = false)
    {
        $result = self::query($sql, $data);

        return $multiple ? $result->fetchAll(\PDO::FETCH_CLASS) : $result->fetchObject();
    }
}