<?php

namespace M1\Core;

class Model
{
    protected $item;

    public function __construct($id = false)
    {
        if (!empty($id)) {
            $this->item = $this->get(false, [
                'id' => $id
            ]);
        }
    }

    public function exist()
    {
        return $this->item;
    }

    public function count()
    {
        $result = DB::select("SELECT COUNT(*) ct FROM `{$this->table}`");

        return $result->ct;
    }

    public function get($fields, $conditions = [], $order = 'id ASC', $offset = 0, $limit = 3, $multiple = false)
    {
        $query = 'SELECT ' . (empty($fields) ? '*' : implode(',', $fields));
        $query .= " FROM `{$this->table}`";

        if (!empty($conditions)) {
            $query .= ' WHERE ' . implode(' AND ', array_map(function ($field) {
                return "`{$field}` = ?";
            }, array_keys($conditions)));
        }

        if ($multiple) {
            if (!empty($order)) {
                $query .= " ORDER BY {$order}";
            }

            $query .= " LIMIT {$offset}, {$limit}";
        }

        return DB::select($query, array_values($conditions), $multiple);
    }

    public function create($data)
    {
        $query = "INSERT INTO {$this->table} (" . implode(',', array_keys($data)) . ') VALUES (' . implode(',', array_map(function () {
            return '?';
        }, $data)) . ')';

        return DB::insert($query, array_values($data));
    }

    public function update($data)
    {
        $query = "UPDATE `{$this->table}` SET " . implode(', ', array_map(function ($field) {
            return "`{$field}` = ?";
        }, array_keys($data))) . " WHERE id = {$this->item->id}";

        return DB::query($query, array_values($data));
    }

    public function delete()
    {
        return DB::query("DELETE FROM {$this->table} WHERE id = {$this->item->id}");
    }
}