<?php

namespace M1\Core;

use Exception;

class Core
{
    static function init()
    {
        try {
            DB::connect();

            $route = !empty($_GET['route']) ? $_GET['route'] : 'main';
            $controllerName = ucfirst($route) . 'Controller';
            $controller = "\\M1\\Controllers\\{$controllerName}";

            if (!class_exists($controller)) {
                throw new Exception("Контроллер {$controllerName} не найден.");
            }

            $controller = new $controller();

            $controller->render();
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
}