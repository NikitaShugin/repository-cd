<?php

namespace M1\Helpers;

class ImageUpload
{
    public static function upload()
    {
        if ($_FILES['file']['error'] > 0) {
            Request::error("Ошибка при сохранении файла: {$_FILES['file']['error']}");
        }

        $extensions = [
            'image/jpeg' => 'jpeg',
            'image/png' => 'png'
        ];

        $type = $_FILES['file']['type'];

        if (!array_key_exists($type, $extensions)) {
            Request::error("Тип файла: {$type}. Разрешённые типы: " . implode(', ', array_values($extensions)) . '. Повторите ещё раз.');
        }

        $name = md5(time() . $_FILES['file']['name']) . ".{$extensions[$type]}";
        $move = move_uploaded_file($_FILES['file']['tmp_name'], UPLOAD_DIR . $name);

        if (!$move) {
            Request::error('Файл не сохранён. Повторите ещё раз.');
        }

        return $name;
    }
}