<?php

namespace M1\Helpers;

class Converter
{
    public static function toString($field)
    {
        return htmlspecialchars($field);
    }

    public static function toInt($field)
    {
        return intval($field);
    }

    public static function toFloat($field)
    {
        return floatval($field);
    }
}