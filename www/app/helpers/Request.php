<?php

namespace M1\Helpers;

class Request
{
    public static function error($message, $data = [])
    {
        self::response(array_merge([
            'error' => $message
        ], $data));
    }

    public static function success($message, $data = [])
    {
        self::response(array_merge([
            'success' => $message
        ], $data));
    }

    public static function response($data)
    {
        header('Content-Type: application/json');

        die(json_encode($data));
    }

    public static function checkErrors($fields)
    {
        $errors = [];

        foreach ($fields as $field) {
            if (empty($_POST[$field])) {
                $errors[] = $field;
            }
        }

        return $errors;
    }
}