<?php

define('DS', DIRECTORY_SEPARATOR);
define('DIR',__DIR__ . DS);
define('TEMPLATE_DIR', DIR . 'views' . DS);
define('UPLOAD_DIR', DIR . 'public' . DS . 'uploads' . DS);