import {Tools} from "./tools.js";

export const App = {
    albumTable: false,

    init: function() {
        this.albumTable = $('#albums-table').DataTable({
            processing: true,
            pageLength: 3,
            serverSide: true,
            bFilter: true,
            pagingType: 'numbers',
            lengthChange: false,
            order: [],
            rowId: 'id',
            language: {
                url: '//cdn.datatables.net/plug-ins/1.10.20/i18n/Russian.json'
            },
            aoColumnDefs: [{
                bSortable: false,
                aTargets: ['sorting-disabled']
            }],
            ajax:  {
                url: '?route=main',
                type: 'POST',
                data: {
                    action: 'index'
                }
            },
            columns: [
                {
                    data: 'image',
                    render: function (data, type, row) {
                        return row.image !== null ? `<img class="row-img" src="uploads/${row.image}">` : '-';
                    }
                },
                {data: 'name'},
                {data: 'author'},
                {
                    data: 'release_year',
                    defaultContent: '-'
                },
                {data: 'duration'},
                {
                    data: 'bought_at',
                    defaultContent: '-'
                },
                {data: 'price'},
                {
                    data: 'room',
                    defaultContent: '-'
                },
                {
                    data: 'stand',
                    defaultContent: '-'
                },
                {
                    data: 'shelf',
                    defaultContent: '-'
                },
                {
                    data: null,
                    defaultContent: '<button class="btn btn-outline-primary" data-toggle="modal" data-target="#update-album"><i class="fa fa-pencil"></button>',
                    orderable: false
                },
                {
                    data: null,
                    defaultContent: '<button class="btn btn-outline-danger delete-button" data-toggle="modal" data-target="#delete-album"><i class="fa fa-trash"></button>',
                    orderable: false
                }
            ],
            initComplete: function () {
                this.api().columns([1, 2, 3, 4, 5, 6, 7, 8, 9]).every( function () {
                    let column = this;
                    let select = $('<select><option value=""></option></select>')
                        .appendTo($(column.footer()).empty())
                        .on('change', function () {
                            column.search($(this).val() ? $(this).val() : '', true, false).draw();
                        });

                    column.data().unique().sort().each(function (d) {
                        if (d) {
                            select.append(`<option value="${d}">${d}</option>`)
                        }
                    });
                } );
            }
        });

        $(document).on('submit', '#create-album form', function () {
            Tools.sendForm(this, function (msg) {
                $('#create-album').modal('hide');

                App.albumTable.row.add(msg.album).draw();
            });

            return false;
        });

        $(document).on('shown.bs.modal', '#delete-album', function (e) {
            const id = App.albumTable.row(e.relatedTarget.closest('tr')).id();

            $(this).find('form [name="id"]').val(id);
        });

        $(document).on('submit', '#delete-album form', function () {
            Tools.sendForm(this, function () {
                $('#delete-album').modal('hide');

                App.albumTable.ajax.reload();
            });

            return false;
        });

        $(document).on('shown.bs.modal', '#update-album', function (e) {
            const data = App.albumTable.row(e.relatedTarget.closest('tr')).data();

            for (let key in data) {
                let field = $(this).find(`[name="${key}"]`);

                if (field.length) {
                    field.val(data[key]);
                }
            }
        });

        $(document).on('submit', '#update-album form', function () {
            Tools.sendForm(this, function () {
                $('#update-album').modal('hide');

                App.albumTable.ajax.reload();
            });

            return false;
        });
    }
};