export const Tools = {
    sendForm: function (form, callback = function () {}) {
        const data = new FormData(form);
        const $form = $(form);
        const action = $form.attr('action');

        $form.find('.is-invalid').removeClass('is-invalid');

        this.ajax(data, function (msg) {
            if (msg.error) {
                Tools.toast(msg);

                if (msg.fields) {
                    for (let name of msg.fields) {
                        let field = $form.find(`[name="${name}"]`);

                        if (field.length) {
                            field.addClass('is-invalid');
                        }
                    }
                }
            } else {
                if (msg.success) {
                    Tools.toast(msg);
                }

                $form.trigger('reset');

                callback(msg);
            }

        }, action !== undefined ? action : '?');
    },

    ajax: function (data, callback = function () {}, url = '?') {
        try {
            let ajaxData = {
                url: url,
                type: 'POST',
                dataType: 'json',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                success: callback
            };

            $.ajax(ajaxData);
        } catch (e) {

        }
    },

    toast: function (message) {
        if (message.success) {
            toastr.success(message.success, 'Отлично!', {
                positionClass: 'toast-bottom-right',
                containerId: 'toast-bottom-right'
            });
        } else if (message.error) {
            toastr.error(message.error, 'Ошибка!', {
                positionClass: 'toast-bottom-right',
                containerId: 'toast-bottom-right'
            });
        }
    }
};